# frozen_string_literal: true

RSpec.describe Api::V1::HealthCheckController do

  describe 'GET /status' do
    subject(:get_status) { get '/api/v1/status' }

    it 'success response' do
      get_status
      
      expect(response).to have_http_status 200
    end

    it 'returns basic information about the system' do
      get_status

      json_response = JSON.parse(response.body).symbolize_keys
      expect(json_response).to eq({ status: 'OK' })
    end
  end
end

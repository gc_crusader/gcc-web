# frozen_string_literal: true

module Api
  module V1
    # Controller for checking health status
    class HealthCheckController < ApplicationController
      def status
        render json: { status: 'OK' }, status: 200
      end
    end
  end
end
